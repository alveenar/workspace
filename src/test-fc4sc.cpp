//============================================================================
// Name        : test-fc4sc.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdint.h>
#include <sys/types.h>
#include <iostream>
using namespace std;
#include "fc4sc.hpp"



int sum(int a,int b){
	return (a+b);
}

template <typename T>
class test_cvg: public covergroup {

public:
	T input_value_1;
	T input_value_2;
	T output_value;
	CG_CONS(test_cvg){};  //constructor


	COVERPOINT(T,input_values_cvp_1,input_value_1){
		bin<T>("low1",1),
		bin<T>("med1",2),
		bin<T>("med2",3),
		bin<T>("high",4)

		};

	COVERPOINT(T,input_values_cvp_2,input_value_2){
		bin<T>("low1",5),
		bin<T>("med1",6),
		bin<T>("med2",7),
		bin<T>("high",8)

		};


	COVERPOINT(T,output_values_cvp,output_value){
		bin<T>("low1",6),
		bin<T>("low1",9),
		bin<T>("med1",8),
		bin<T>("med2",15),
		bin<T>("high",20)

		};

	//cross of the defined I/O coverpoints
	//cross<int, int> data_cross= cross<int, int> (
	cross<int, int> data_cross = cross<int, int> (
		this,
		&input_values_cvp_1,
		//&input_values_cvp_2,
		&output_values_cvp);



};





int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

	test_cvg<int> cvg_inst; //class instance

	for(int i=0;i<5;i++){

		//cvg_inst.input_value_1=rand()%100;
		//cvg_inst.input_value_2=rand()%100;
		cvg_inst.input_value_1 = i;
		cvg_inst.input_value_2 = i + 4;

		cvg_inst.output_value=sum(cvg_inst.input_value_1,cvg_inst.input_value_2);

		cvg_inst.sample(); //must be called every time new value is assigned

	}

	std::string name("coverage_test.xml");
	fc4sc::global::coverage_save(name);


	return 0;
}
